# Visión y alcance

Para el Grupo de Ciencia de Datos
que está buscando desarrollar proyectos de investigación
el Programa Integral de Seguridad informática
es un sistema de generación de información
que permite explorar los patrones generados por ataques informáticos en condiciones similares a las infraestructuras de producción
y a diferencia de otros planes de trabajo para investigación
se integra con la labor docente y la simplifica,
al tiempo que permite entrenar equipos para competir en el CSAW.

Este proyecto aprovecha la información generada en la asignatura «Ciberseguridad y Laboratorio»
para generar sistemas de aprendizaje de máquina que identifiquen amenazas
y ofrece oportunidades para que los estudiantes participen en proyectos de investigación.

<!--

# Requerimientos del proyecto

## Antecedentes

## Oportunidad

## Objetivos de proyecto

## Métricas de éxito

## Declaración de la visión

## Riesgos del proyecto

## Suposiciones del proyecto y dependencias

# Alcance y limitaciones

## Características principales

## Alcance de la versión inicial

Proponer la idea al Grupo de Ciencia de Datos y el Director del DEII.

## Alcance de las versiones siguientes



## Limitaciones y exclusiones

# Contexto de proyecto

## Perfiles de involucrados

## Prioridades del proyecto

[¿Cuáles de los siguientes factores son limitantes, impulsores o flexibles?]

- [Características]
- [Calidad]
- [Tiempo]
- [Costo]
- [Personal]

## Consideraciones de implementación

-->

**Alcance**: Presentar propuesta de trabajo

# Productos posibles

-   [Planeación Didáctica para asignatura «Ciberseguridad y Laboratorio»](https://docs.google.com/spreadsheets/d/1Z4iwDFmcIA5q6yR6w31ohtRxipHZ3tWJf5Lcs7gUpTM/edit?usp=sharing ).

    Permite una dinámica de aprender haciendo.

-   Infraestructura personalizada para trabajo y calificación de ciberseguridad.

    Por la diversidad de la configuración representa un caso único para cada persona,
    dificultando la deshonestidad académica.

-   Estudios de la dinámica de aprendizaje en materia de seguridad informática.

    Usar métricas automatizadas para estudiar el fenómeno del aprendizaje
    podría llevar a desarrollo de metodologías de enseñanza
    y publicaciones en materia educativa.

-   Metodología de instrumentación y monitoreo para progreso de estudiantes.

    Usar una metodología que permita medir el esfuerzo
    y eficacia de las actividades de aprendizaje en los estudiantes.

-   Registros de sistemas bajo ataque sistemático por personal junior de ciberseguridad.

    La información podría usarse para entrenar sistema de aprendizaje de máquina.

-   Sistema de aprendizaje de máquina que detecta patrones de ataque comunes.

    Este sistema podría patentarse y posiblemente representar una fuente de ingresos para la Universidad.

-   Patentes.

    De generarse sistemas productivos o metodologías,
    éstas podrían representar fuentes de ingreso para la institución.

-   Formación de personal especializado en ciberseguridad.

    Este personal se entrenaría de manera práctica
    con el objetivo de encontrar vulnerabilidades en software usado en sistemas de producción.

-   Premios en concursos de Ciberseguridad.

    De realizarse, esto podría traer prestigio a la institución,
    atraer prensa,
    atraer estudiantes,
    etc…

-   Reportes de Vulnerabilidad públicos.

    De realizarse, esto podría traer prestigio a la institución,
    atraer prensa,
    atraer estudiantes,
    etc…

# Actores y sus intereses

## Usuarios favorecidos

-   Estudiantes de preparatoria interesados en la ciberseguridad:

-   Estudiantes de «Ciberseguridad y Laboratorio» (Diego):

-   Director del Departamento de Ingeniería e Innovación (Edgar):
    -   Posicionar el departamento y la institución como la vanguardia nacional en Ciberseguridad.
    -   Posicionar el departamento y la institución como la vanguardia nacional en Ciencia de datos.

-   Académicos del Grupo de Ciencia de Datos (César, Joshua):
    -   Atraer proyectos de investigación y colaboración con estudiantes e investigadores.
    -   Simplificar la evaluación de la asignatura «Ciberseguridad y Laboratorio».
    -   Presentar evidencia para la mejora continua de las metodologías de enseñanza.

[Interés de quien usará el producto] (Campeón o persona del grupo)

## Usuarios desfavorecidos

[¿De quién tengo que cuidarme? | Usuario desfavorecido | Bot]: [Interés de grupos adversarios que deben restringirse]

## Usuarios ignorados

[Usuario ignorado]: [Puede usar el producto pero no se alinea a los objetivos de negocio]

## Otros Usuario

[Otros usuarios | bots]: [¿qué quieren?]

## Otros beneficiados

[¿A quién más le beneficia el producto?]: [¿Qué quiere esa persona?].

## Otros afectados

-   La Universidad Iberoamericana como persona moral:

    -   Mantener o mejorar su imagen y reputación como institución de prestigio.
    -   Evitar problemas legales, éticos o propagandísticos.
    -   Posicionarse como una institución de élite en materia de Seguridad Informática.
    -   Posicionarse como una institución de élite en materia de Ciencia de Datos.

[¿A quién más le afecta el producto?]: [¿Qué quiere esa persona?].

# Glosario

[Describir cada término que se usa en la especificación.]

# Documentación suplementaria

- [Ideario](https://ibero.mx/sites/all/themes/ibero/descargables/acercade/Ideario.pdf )
- [Perfil del egresado](https://ibero.mx/sites/all/themes/ibero/descargables/corpus/008.pdf )
- [Reglamento universitario](https://ibero.mx/corpus-reglamentario )
