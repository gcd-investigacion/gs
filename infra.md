La infraestructura debe tener varios servicios para explotar las vulnerabilidades más comunes.

La arquitectura debe incluir:

-   DNS
-   Base de datos
-   Servicios Web
-   Firewall
-   Aplicación
-   Sistema de logs externo
-   Sistema de monitoreo
-   Análisis de vulnerabilidades
-   Ambiente de desarrollo

-   Servicio de banco ficticio que valida los tokens obtenidos.

Para utilizar el método:

-   Exploración
-   Enumeración
-   Explotación

Para explorar cómputo forense.

Para explorar las vulnerabilidades:

-   DNS migración
-   Escucha de protocolos inseguros
-   SQL injection
-   Stack Overflow
-   Denegación de servicio
-   Robo de sesión
-   Race conditions

Es preferible que tenga capacidad de variabilidad
para validar habilidades a través de tokens únicos por estudiante.
Las fuentes de variabilidad:

-   Tokens únicos por estudiante
-   Puertos distintos
-   Servicios diferentes
-   Vulnerabilidades implementadas
